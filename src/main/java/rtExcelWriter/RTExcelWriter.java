package rtExcelWriter;

import model.AnalyzedRTData;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class RTExcelWriter {
    private Workbook workbook;

    public RTExcelWriter() {
        this.workbook = new XSSFWorkbook();
    }

    public void createSheet(String name) {
        workbook.createSheet(name);
    }

    public void writeAnalyzedRTData(String sheetName, List<List<AnalyzedRTData>> data) {

        final String[] COLUMNS = { "Sample Name", "Ct value", "dCt", "ddCt", "2^-ddCt" };
        int rowCounter = 0;

        Sheet sheet = workbook.getSheet(sheetName);

        for (List<AnalyzedRTData> list : data) {
            // Create header

            // Target name
            Row rowHeaderTarget = sheet.createRow(rowCounter++);
            Cell cellHeaderTarget = rowHeaderTarget.createCell(0);
            cellHeaderTarget.setCellValue(list.get(0).getTarget());

            // COLUMN headers
            Row rowHeaderColumns = sheet.createRow(rowCounter++);
            for (int j = 0; j < COLUMNS.length; ++j) {
                Cell cell = rowHeaderColumns.createCell(j);
                cell.setCellValue(COLUMNS[j]);
            }

            // Data
            for (int j = 0; j < list.size(); ++j) {

                AnalyzedRTData datum = list.get(j);

                Row rowData = sheet.createRow(rowCounter++);

                Cell cell0 = rowData.createCell(0);
                cell0.setCellValue(datum.getSample());

                Cell cell1 = rowData.createCell(1);
                cell1.setCellValue(datum.getCq());

                Cell cell2 = rowData.createCell(2);
                cell2.setCellValue(datum.getCt_delta());

                Cell cell3 = rowData.createCell(3);
                cell3.setCellValue(datum.getCt_double_delta());

                Cell cell4 = rowData.createCell(4);
                cell4.setCellValue(datum.getCt_two_to_the_power_of_neg_double_delta());
            }

            sheet.createRow(rowCounter++);
        }
    }

    public void write(final String path) throws IOException {
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(path);
        workbook.write(fileOut);
        fileOut.close();
    }

    public void close() throws IOException {
        // Closing the workbook
        workbook.close();
    }

}
