package rtAnalyzer;

import model.AnalyzedRTData;
import model.BaseRTData;

import java.util.ArrayList;
import java.util.List;

public class RTAnalyzer {

    public static <T extends BaseRTData> List<T> calculateAvgCq(final List<? extends BaseRTData> data, final Class<T> model) {

        List<T> analyzedList = new ArrayList();
        final int TEST_SET_SIZE = retrieveTestSetSize(data);

        // get avg cq
        double sampleTargeAvgCq = 0;
        int counter = 0;

        for (int i = 0; i < data.size(); ++i) {
            sampleTargeAvgCq += data.get(i).getCq();
            ++counter;

            if (counter == TEST_SET_SIZE) {
                sampleTargeAvgCq /= TEST_SET_SIZE;

                try {
                    T analyzed = RTTransformer.transform(data.get(i), model);
                    analyzed.setCq(sampleTargeAvgCq);
                    analyzedList.add(analyzed);
                } catch (Exception ex) {
                    ex.printStackTrace();;
                }

                counter = 0;
                sampleTargeAvgCq = 0;
            }
        }

        return analyzedList;
    }

    public static List<AnalyzedRTData> sortByTargetThenSampleThenControl(final List<AnalyzedRTData> analyzedRTData) {

        analyzedRTData.sort( (o1, o2) -> {

            // sort by target

            int target = o1.getTarget().compareTo(o2.getTarget());
            if (target == 0) {

                // then day
                int day = new Integer(extractSampleDay(o1.getSample())).compareTo(new Integer( extractSampleDay(o2.getSample()) ));
                if (day == 0) {

                    int c1 = isSampleControl(o1.getSample()) ? 1 : 0;
                    int c2 = isSampleControl(o2.getSample()) ? 1 : 0;

                    if (c1 < c2) {
                        return 1;
                    } else if (c1 > c2) {
                        return -1;
                    }

                } else {
                    return day;
                }
            }
            return target;
        });

        extractSampleDay(analyzedRTData.get(0).getSample());

        return analyzedRTData;
    }

    public static String[] extractSample(final String sample) {
        // split by ' ' or ',' or '-' or '_'    -> possible choices
        final String[] SEPARATED_CHOICES = {" ", ",", "-", "_"};

        String[] splitStr = null;

        for (String choice : SEPARATED_CHOICES) {
            splitStr = sample.split(choice);

            if (splitStr.length > 1) break;
        }

        return splitStr;
    }

    public static int extractSampleDay(final String sample) {

        String[] splitStr = extractSample(sample);

        // find the day
        int day = -1;
        for (String ele : splitStr) {
            if (ele.contains("D") || ele.contains("d")) {
                day = Integer.parseInt( ele.replaceAll("[^0-9]", "") );
                break;
            }
        }

        return day;
    }

    public static boolean isSampleControl(final String sample) {

        String[] splitStr = extractSample(sample);

        // find the day
        boolean isControl = false;
        for (String ele : splitStr) {
            if (ele.contains("Co") || ele.contains("co")) {
                isControl = true;
                break;
            }
        }

        return isControl;
    }

    public static String extractSampleControl(final String sample) {

        String[] splitStr = extractSample(sample);

        // find the day
        String ctr = "";
        for (String ele : splitStr) {
            if (ele.contains("Co") || ele.contains("co")) {
                ctr = ele;
                break;
            }
        }

        return ctr;
    }

    public static List<String> retrieveDistinctTarget(final List<AnalyzedRTData> sortedRest) {
        List<String> distinctTarget = new ArrayList<>();

        String currDTarget = "";
        for (AnalyzedRTData d : sortedRest) {
            if (!currDTarget.equals(d.getTarget())) { // distinct
                distinctTarget.add(d.getTarget());
                currDTarget = d.getTarget();
            }
        }

        return distinctTarget;
    }

    private static int retrieveTestSetSize(final List<? extends BaseRTData> data) {

        String initialTarget = data.get(0).getTarget();

        for (int i = 1; i < data.size(); ++i) {
            if (!initialTarget.equals(data.get(i).getTarget())) {
                return i;
            }
        }

        return -1;
    }

}
