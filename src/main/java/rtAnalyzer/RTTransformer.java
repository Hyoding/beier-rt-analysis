package rtAnalyzer;

import model.BaseRTData;

import java.util.ArrayList;
import java.util.List;

public class RTTransformer {

    public static <T extends BaseRTData> T transform(final BaseRTData data, final Class<T> model) throws IllegalAccessException, InstantiationException {
        T transformedData = model.newInstance();

        transformedData.setSample(data.getSample());
        transformedData.setTarget(data.getTarget());

        return model.cast(transformedData);
    }

    public static <T extends BaseRTData> List<T> transform(final List<BaseRTData> data, final Class<T> model) {
        List<T> collection = new ArrayList<T>();

        data.forEach((d) -> {

            try {
                collection.add(transform(d, model));
            } catch(Exception ex) {}

        });

        return collection;
    }
}
