package rtExcelParser;

import model.bioradCF96.BioRadCF96RawRTData;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.Iterator;

public class BioRadCFX96RTExcelParser extends BaseRTExcelParser {

    private final String FIRST_ROW_FIRST_COL = "Well";

    public BioRadCFX96RTExcelParser(final String path) {
        super(path);
    }

    private boolean canParse(Row row) {
        return (row.getLastCellNum() - row.getFirstCellNum()) >= 15;
    }

    @Override
    protected <T> T parseRow(Iterator<Row> rowIt, final Class<T> model) {
        Row row = rowIt.next();

        if (canParse(row)) {
            // iterate on cells for the current row
            Iterator<Cell> cellIterator = row.cellIterator();
            BioRadCF96RawRTData data = new BioRadCF96RawRTData();

            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                if (cell.toString().equals(FIRST_ROW_FIRST_COL)) { // skip first row, it is table header
                    data = null;
                    break;
                } else {

                    switch (cell.getColumnIndex()) {
                        case 1:
                            data.setWell(cell.toString());
                            break;

                        case 2:
                            data.setFluor(cell.toString());
                            break;

                        case 3:
                            data.setTarget(cell.toString());
                            break;

                        case 4:
                            data.setContent(cell.toString());
                            break;

                        case 5:
                            data.setSample(cell.toString());
                            break;

                        case 7:
                            data.setCq(Double.parseDouble(cell.toString()));
                            break;

                        case 8:
                            data.setCqMean(Double.parseDouble(cell.toString()));
                            break;

                        case 9:
                            data.setCqStdDev(Double.parseDouble(cell.toString()));
                            break;

                        default:
                            break;
                    }
                }
            }

            return model.cast(data);
        } else {
            return null;
        }
    }
}
