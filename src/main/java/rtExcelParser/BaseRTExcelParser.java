package rtExcelParser;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

abstract public class BaseRTExcelParser {
    private String path;

    private XSSFWorkbook workbook;
    private FileInputStream fis;
    private int currentSheetIdx;

    public BaseRTExcelParser(final String path) {
        this.path = path;
        this.currentSheetIdx = 0;
    }

    abstract protected <T> T parseRow(Iterator<Row> rowIt, final Class<T> model);

    public void init() throws IOException {
        File excelFile = new File(path);
        fis = new FileInputStream(excelFile);
        workbook = new XSSFWorkbook(fis);
    }


    // better do this per sheet
    public <T > List<T> parse(final Class<T> model) {
        ArrayList<T> rawData = new ArrayList<T>();

        XSSFSheet sheet = workbook.getSheetAt(currentSheetIdx);
        Iterator<Row> rowIt = sheet.iterator();

        while(rowIt.hasNext()) {
            T parsedData = parseRow(rowIt, model);
            if (parsedData != null) {
                rawData.add(parsedData);
            } else {
                // don't add this to the list
            }
        }

        return rawData;
    }

    public void terminate() throws IOException {
        workbook.close();
        fis.close();
    }

    // TODO: Sort by sample then target. Use BaseRTData obj for sorting
    public void sort() {

    }

    public boolean hasNextSheet() {
        try {
            XSSFSheet sheet = workbook.getSheetAt(currentSheetIdx + 1);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public void nextPage() {
        ++currentSheetIdx;
    }

    public String getCurrentSheetName() {
        return workbook.getSheetName(currentSheetIdx);
    }

    public int getCurrentSheetIdx() {
        return currentSheetIdx;
    }
}
