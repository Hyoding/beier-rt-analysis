package model.bioradCF96;

import model.BaseRTData;

public class BioRadCF96RawRTData extends BaseRTData {

    private String well;
    private String fluor;
    private String content;
    private double cqMean;
    private double cqStdDev;

    // optional?
    private String biologicalSetName;
    private int startingQuantity;
    private int logStartingQuantity;
    private double sqMean;
    private double sqStdDev;
    private double setPoint;
    private String wellNode;

    public BioRadCF96RawRTData() {}

    public BioRadCF96RawRTData(final String well, final String fluor, final String target, final String content, final String sample, final String biologicalSetName, final double cq, final double cqMean, final double cqStdDev, final int startingQuantity, final int logStartingQuantity, final double sqMean, final double sqStdDev, final double setPoint, final String wellNode) {
        super(target, sample, cq);
        this.well = well;
        this.fluor = fluor;
        this.content = content;
        this.cqMean = cqMean;
        this.cqStdDev = cqStdDev;

        // optional
        this.biologicalSetName = biologicalSetName;
        this.startingQuantity = startingQuantity;
        this.logStartingQuantity = logStartingQuantity;
        this.sqMean = sqMean;
        this.sqStdDev = sqStdDev;
        this.setPoint = setPoint;
        this.wellNode = wellNode;
    }

    public String getWell() {
        return well;
    }

    public void setWell(String well) {
        this.well = well;
    }

    public String getFluor() {
        return fluor;
    }

    public void setFluor(String fluor) {
        this.fluor = fluor;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getCqMean() {
        return cqMean;
    }

    public void setCqMean(double cqMean) {
        this.cqMean = cqMean;
    }

    public double getCqStdDev() {
        return cqStdDev;
    }

    public void setCqStdDev(double cqStdDev) {
        this.cqStdDev = cqStdDev;
    }

    public String getBiologicalSetName() {
        return biologicalSetName;
    }

    public void setBiologicalSetName(String biologicalSetName) {
        this.biologicalSetName = biologicalSetName;
    }

    public int getStartingQuantity() {
        return startingQuantity;
    }

    public void setStartingQuantity(int startingQuantity) {
        this.startingQuantity = startingQuantity;
    }

    public int getLogStartingQuantity() {
        return logStartingQuantity;
    }

    public void setLogStartingQuantity(int logStartingQuantity) {
        this.logStartingQuantity = logStartingQuantity;
    }

    public double getSqMean() {
        return sqMean;
    }

    public void setSqMean(double sqMean) {
        this.sqMean = sqMean;
    }

    public double getSqStdDev() {
        return sqStdDev;
    }

    public void setSqStdDev(double sqStdDev) {
        this.sqStdDev = sqStdDev;
    }

    public double getSetPoint() {
        return setPoint;
    }

    public void setSetPoint(double setPoint) {
        this.setPoint = setPoint;
    }

    public String getWellNode() {
        return wellNode;
    }

    public void setWellNode(String wellNode) {
        this.wellNode = wellNode;
    }
}
