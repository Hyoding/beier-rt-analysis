package model;

public class BaseRTData {
    protected String target;
    protected String sample;
    protected double cq;

    public BaseRTData() {}

    public BaseRTData(String target, String sample, double cq) {
        this.target = target;
        this.sample = sample;
        this.cq = cq;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSample() {
        return sample;
    }

    public void setSample(String sample) {
        this.sample = sample;
    }

    public double getCq() {
        return cq;
    }

    public void setCq(double cq) {
        this.cq = cq;
    }
}
