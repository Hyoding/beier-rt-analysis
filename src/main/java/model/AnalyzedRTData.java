package model;

public class AnalyzedRTData extends BaseRTData {
    private String houseKeeping_gene;
    private double ct_delta;
    private double ct_double_delta;
    private double ct_two_to_the_power_of_neg_double_delta;

    public AnalyzedRTData() {}

    public AnalyzedRTData(String target, String sample, double cq, String houseKeeping_gene, double ct_delta, double ct_double_delta, double ct_two_to_the_power_of_neg_double_delta) {
        super(target, sample, cq);
        this.houseKeeping_gene = houseKeeping_gene;
        this.ct_delta = ct_delta;
        this.ct_double_delta = ct_double_delta;
        this.ct_two_to_the_power_of_neg_double_delta = ct_two_to_the_power_of_neg_double_delta;
    }

    public String getHouseKeeping_gene() {
        return houseKeeping_gene;
    }

    public void setHouseKeeping_gene(String houseKeeping_gene) {
        this.houseKeeping_gene = houseKeeping_gene;
    }

    public double getCt_delta() {
        return ct_delta;
    }

    public void setCt_delta(double ct_delta) {
        this.ct_delta = ct_delta;
    }

    public double getCt_double_delta() {
        return ct_double_delta;
    }

    public void setCt_double_delta(double ct_double_delta) {
        this.ct_double_delta = ct_double_delta;
    }

    public double getCt_two_to_the_power_of_neg_double_delta() {
        return ct_two_to_the_power_of_neg_double_delta;
    }

    public void setCt_two_to_the_power_of_neg_double_delta(double ct_two_to_the_power_of_neg_double_delta) {
        this.ct_two_to_the_power_of_neg_double_delta = ct_two_to_the_power_of_neg_double_delta;
    }
}
