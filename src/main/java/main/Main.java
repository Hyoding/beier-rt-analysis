package main;

import rtExcelParser.BioRadCFX96RTExcelParser;
import model.AnalyzedRTData;
import model.bioradCF96.BioRadCF96RawRTData;
import rtAnalyzer.RTAnalyzer;
import rtExcelWriter.RTExcelWriter;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static rtAnalyzer.RTAnalyzer.*;

public class Main {

    private static String HK_GENE;

    public static void main(String[] args) throws IOException {

        // take in path from user
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter file name: ");
        final String path = sc.nextLine();

        // take in homekeeping gene
        System.out.print("Please enter home keeping gene: ");
        HK_GENE = sc.nextLine();

//        String path = "/Users/josephryu/repos/beier/beierrtanalysis/src/main/resources/stub/1 - 2018-08-10 16-06-54_T7 micromass cDNA lipid genes -  Quantification Cq Results.xlsx";
//        HK_GENE = "Gapdh";

        BioRadCFX96RTExcelParser parser = new BioRadCFX96RTExcelParser(path);
        RTExcelWriter excelWriter = new RTExcelWriter();

        try {
            parser.init();
        } catch (Exception ex) {
            System.out.println("File doesn't exist or is not valid. Please check the file again.");
            System.exit(0);
        }

        System.out.println("\nParsing the file..");


        for (;;) {

            List<BioRadCF96RawRTData> parsedData = parser.parse(BioRadCF96RawRTData.class);

            if (parsedData.isEmpty()) break;

            System.out.println("Analyzing file..");

            List<AnalyzedRTData> convertedDataWithAvgCq = RTAnalyzer.calculateAvgCq(parsedData, AnalyzedRTData.class);
            List<List<AnalyzedRTData>> analyzed = analyzeBioRadCF96(convertedDataWithAvgCq);

            String sheetName = "result " + parser.getCurrentSheetIdx();
            excelWriter.createSheet(sheetName);

            excelWriter.writeAnalyzedRTData(sheetName, analyzed);

            if (parser.hasNextSheet()) {
                parser.nextPage();
            } else {
                break;
            }
        }

        try {
            final String writePath = "result-" + new java.util.Date() + ".xlsx";
            excelWriter.write(writePath);
            parser.terminate();

            System.out.println("\nDone: " +writePath);
        } catch (Exception ex) {
        }
    }

    private static List<List<AnalyzedRTData>> analyzeBioRadCF96(final List<AnalyzedRTData> analyzedRTData) {

        // Check if HK_GENE actually exists
        boolean hkExists = analyzedRTData.stream().filter( d -> d.getTarget().equals(HK_GENE)).findFirst().isPresent();
        if (!hkExists) {
            System.out.println("House Keeping gene does not exist in the data set");
            System.exit(0);
        }

        // sort by target then sample name
        List<AnalyzedRTData> sorted = sortByTargetThenSampleThenControl(analyzedRTData);

        // put hk_gene aside
        List<AnalyzedRTData> sortedHK = sorted.stream().filter(d -> d.getTarget().equals(HK_GENE)).collect(Collectors.toList());
        List<AnalyzedRTData> sortedRest = sorted.stream().filter(d -> !d.getTarget().equals(HK_GENE)).collect(Collectors.toList());

        List<List<AnalyzedRTData>> organizedAnalyzedData = new ArrayList<>();
        organizedAnalyzedData.add(sortedHK);

        for ( String target : retrieveDistinctTarget(sortedRest) ) {
            List<AnalyzedRTData> list = sortedRest.stream().filter(d -> d.getTarget().equals(target)).collect(Collectors.toList());
            final double INIT_CT_DELTA = list.get(0).getCq() - sortedHK.get(0).getCq();

            list.forEach( d -> {
                final double HK_CT = sortedHK.stream().filter( hk -> d.getSample().equals(hk.getSample())).findFirst().get().getCq();

                d.setCt_delta( d.getCq() - HK_CT );
                d.setCt_double_delta( d.getCt_delta() - INIT_CT_DELTA );
                d.setCt_two_to_the_power_of_neg_double_delta(Math.pow(2, -d.getCt_double_delta()));
            });

            organizedAnalyzedData.add(list);
        }

        return organizedAnalyzedData;
    }
}
